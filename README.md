# Pelcro Authorization Module

This module connects with Pelcro provided environmental javascript on the
frontend and via the Pelcro API on the backend to provide seamless logging into
Drupal via Pelcro.
