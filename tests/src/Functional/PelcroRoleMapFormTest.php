<?php

namespace Drupal\Tests\pelcro_auth\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\Entity\User;

/**
 * Tests the Pelcro RoleMap Form.
 *
 * @group pelcro_auth
 */
class PelcroRoleMapFormTest extends BrowserTestBase {
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'node',
    'field',
    'text',
    'user',
    'pelcro_auth',
    'externalauth',
  ];


  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $account = User::load(1);

    // Reset the password.
    $password = 'foo';
    $account->setPassword($password)->save();

    // Support old and new tests.
    $account->passRaw = $password;

    $this->drupalLogin($account);
  }

  /**
   * Tests that the rolemap form won't load without a site config.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testNoConfig() {
    $this->drupalGet('/admin/config/services/pelcro/rolemap');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('In order to configure role mappings, you must first configure the Pelcro authorization settings');
  }

  /**
   * Tests the rolemap form with mock roles and products.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testConfig() {
    \Drupal::service('module_installer')->install(['pelcro_auth_mock_connector']);
    $this->createRole([], 'member', 'Member');
    $this->drupalGet('/admin/config/services/pelcro/rolemap');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextNotContains('In order to configure role mappings, you must first configure the Pelcro authorization settings');
    $this->assertSession()->pageTextContains('Member');
    $edit = [
      'role_map[12345][member]' => TRUE,
      'entitlement_map[items][0][entitlement]' => 'member_entitlement',
      'entitlement_map[items][0][role_ids][member]' => TRUE,

    ];
    $this->submitForm($edit, 'Save');
    $config = \Drupal::config('pelcro_auth.pelcrorolemap');
    $this->assertEquals([
      0 => [
        'product_id' => 12345,
        'role_ids' => ['member'],
      ],
    ], $config->get('role_map'));
    $this->assertEquals([
      0 => [
        'entitlement' => 'member_entitlement',
        'role_ids' => ['member'],
      ],
    ], $config->get('entitlement_map'));
  }

}
