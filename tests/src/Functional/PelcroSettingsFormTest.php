<?php

namespace Drupal\Tests\pelcro_auth\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;

/**
 * Tests the Pelcro Settings Form.
 *
 * @group pelcro_auth
 */
class PelcroSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'node',
    'field',
    'text',
    'user',
    'pelcro_auth',
    'externalauth',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    FieldStorageConfig::create([
      'id' => 'user.field_first_name',
      'entity_type' => 'user',
      'field_name' => 'field_first_name',
      'type' => 'string',
    ])->save();
    FieldConfig::create([
      'id' => 'user.user.field_first_name',
      'field_name' => 'field_first_name',
      'entity_type' => 'user',
      'bundle' => 'user',
      'label' => 'First Name',
    ])->save();
  }

  /**
   * Tests the settings form.
   */
  public function testSettingsForm() {
    $account = User::load(1);

    // Reset the password.
    $password = 'foo';
    $account->setPassword($password)->save();

    // Support old and new tests.
    $account->passRaw = $password;

    $this->drupalLogin($account);
    $this->drupalGet('/admin/config/services/pelcro');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Pelcro Configuration Settings');
    $edit = [
      'server' => 'www',
      'site_id' => '123',
      'api_key' => 'API_KEY',
      'subscribe_redirect' => 'node/1',
      'environment_ui' => 'http://example.com/test.js',
      'environment_stripe' => 'ENVIRONMENT_STRIPE',
      'enable_name_fields_in_register' => TRUE,
      'field_mappings[field_first_name]' => 'first_name',
    ];
    $this->submitForm($edit, 'Save');
    $config = \Drupal::config('pelcro_auth.pelcrosettings');
    unset($edit['field_mappings[field_first_name]']);
    foreach ($edit as $key => $value) {
      $this->assertEquals($value, $config->get($key));
    }
    $this->assertEquals([
      0 => [
        'field_name' => 'field_first_name',
        'account_key' => 'first_name',
      ],
    ], $config->get('field_mapping'));
  }

}
