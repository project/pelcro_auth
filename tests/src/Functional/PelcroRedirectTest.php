<?php

namespace Drupal\Tests\pelcro_auth\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests access denied redirects.
 *
 * @group pelcro_auth
 */
class PelcroRedirectTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'pelcro_auth',
    'externalauth',
  ];

  protected $defaultTheme = 'stable';

  public function testLogout() {
    $this->drupalGet('/pelcro/logout');
    $this->assertSession()->statusCodeNotEquals(403);
  }
}
