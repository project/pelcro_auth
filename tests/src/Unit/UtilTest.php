<?php

namespace Drupal\Tests\pelcro_auth\Unit;

use Drupal\pelcro_auth\Util;
use Drupal\Tests\UnitTestCase;

/**
 * Test the util class.
 *
 * @group pelcro_auth
 *
 * @coversDefaultClass \Drupal\pelcro_auth\Util
 */
class UtilTest extends UnitTestCase {

  /**
   * Tests the cleanOrigin method.
   *
   * @param string $orig
   *   The original origin.
   *
   * @param string $cleaned
   *   The expected cleaned origin.
   *
   * @covers ::cleanOrigin
   *
   * @dataProvider cleanOriginData
   */
  public function testCleanOrigin(string $orig, string $cleaned) {
    $processed = Util::cleanOrigin($orig);
    $this->assertEquals($cleaned, $processed);
  }

  /**
   * Data provider for testCleanOrigin.
   *
   * @return array
   *   The data.
   */
  public function cleanOriginData(): array {
    return [
      [
        'orig' => 'https://www.example.com',
        'cleaned' => 'https://www.example.com',
      ],
      [
        'orig' => 'https://www.example.com/path/that/is/long',
        'cleaned' => 'https://www.example.com/path/that/is/long',
      ],
      [
        'orig' => 'https://www.example.com/path?q=test&email=test@test.com',
        'cleaned' => 'https://www.example.com/path?q=test&email=test@test.com',
      ],
      [
        'orig' => 'https://www.example.com:2021/path',
        'cleaned' => 'https://www.example.com:2021/path',
      ],
      [
        'orig' => 'https://www.example.com/path?view=random#fragment',
        'cleaned' => 'https://www.example.com/path?view=random#fragment',
      ],
      [
        'orig' => 'https://www.example.com?view=login',
        'cleaned' => 'https://www.example.com/',
      ],
      [
        'orig' => 'https://www.example.com?view=password-reset&email=test@test.com&token=xxx&q=searchstring',
        'cleaned' => 'https://www.example.com/?q=searchstring',
      ],
      [
        'orig' => 'https://www.example.com/path?view=register',
        'cleaned' => 'https://www.example.com/path',
      ],
      [
        'orig' => 'https://www.example.com/path?view=password-forgot',
        'cleaned' => 'https://www.example.com/path',
      ],

    ];
  }

}
