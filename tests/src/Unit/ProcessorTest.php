<?php

namespace Drupal\Tests\pelcro_auth\Unit;

use Drupal\pelcro_auth\PelcroProcessor;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the PelcroProcessor class.
 *
 * @group pelcro_auth
 *
 * @coversDefaultClass \Drupal\pelcro_auth\PelcroProcessor
 */
class ProcessorTest extends UnitTestCase {

  /**
   * Tests entitlements parsing.
   *
   * @param object $data
   *   The Pelcro user data.
   * @param array $expected_entitlements
   *   The expected entitlement array.
   *
   * @covers ::getEntitlements
   *
   * @dataProvider getEntitlementsData
   */
  public function testGetEntitlements(object $data, array $expected_entitlements) {
    $entitlements = PelcroProcessor::getEntitlements($data);
    $this->assertEquals($expected_entitlements, $entitlements);
  }

  /**
   * Provides data for testGetEntitlements.
   *
   * @return array[]
   *   The test data.
   */
  public function getEntitlementsData(): array {
    return [

      'subscription_only' => [
        'data' => (object) [
          'subscriptions' => [
            (object) [
              'status' => 'active',
              'plan' => (object) [
                'entitlements' => [
                  'test_entitlement',
                ],
              ],
            ],
          ],
        ],
        'expected_entitlements' => [
          'test_entitlement',
        ],
      ],

      'membership_only' => [
        'data' => (object) [
          'memberships' => [
            (object) [
              'subscription' => (object) [
                'status' => 'active',
                'plan' => (object) [
                  'entitlements' => [
                    'test_entitlement',
                  ],
                ],
              ],
            ],
          ],
        ],
        'expected_entitlements' => [
          'test_entitlement',
        ],
      ],
    ];
  }

}
