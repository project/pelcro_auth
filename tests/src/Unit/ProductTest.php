<?php

namespace Drupal\Tests\pelcro_auth\Unit;

use Drupal\pelcro_auth\Product;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the product class.
 *
 * @group pelcro_auth
 *
 * @coversDefaultClass \Drupal\pelcro_auth\Product
 */
class ProductTest extends UnitTestCase {

  /**
   * The product object to test.
   *
   * @var \Drupal\pelcro_auth\Product
   */
  protected Product $product;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->product = new Product(1, 'Test Product', 'Test Product Description');
  }

  /**
   * Tests ::getId().
   *
   * @covers ::getId
   */
  public function testGetId() {
    $this->assertEquals(1, $this->product->getId());
  }

  /**
   * Tests ::getName().
   *
   * @covers ::getName
   */
  public function testGetName() {
    $this->assertEquals('Test Product', $this->product->getName());
  }

  /**
   * Tests ::getDescription().
   *
   * @covers ::getDescription
   */
  public function testGetDescription() {
    $this->assertEquals('Test Product Description', $this->product->getDescription());
  }

}
