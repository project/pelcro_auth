<?php

namespace Drupal\pelcro_auth_mock_connector;

use Drupal\pelcro_auth\PelcroConnector as OriginalPelcroConnector;

/**
 * Mock PelcroConnector service.
 */
class PelcroConnector extends OriginalPelcroConnector {

  /**
   * {@inheritdoc}
   */
  public function hasSiteConfig(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function listProducts(): array {
    return [
      (object) [
        'id' => 12345,
        'name' => 'product1',
        'description' => 'Product 1',
      ],
    ];
  }

}
