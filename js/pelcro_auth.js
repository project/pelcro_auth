/**
 *  @file
 **/

(function ($, Drupal, window, document) {

  'use strict'
  var Pelcro = window.Pelcro || (window.Pelcro = {})
  Pelcro.uiSettings = {}
  Pelcro.siteid = drupalSettings.pelcro.siteid
  Pelcro.environment = {}
  Pelcro.environment.stripe = drupalSettings.pelcro.stripe
  Pelcro.environment.domain = drupalSettings.pelcro.domain
  Pelcro.environment.ui = drupalSettings.pelcro.ui
  Pelcro.uiSettings.enableNameFieldsInRegister = drupalSettings.pelcro.enableNameFieldsInRegister

})(jQuery, Drupal, this, this.document);
