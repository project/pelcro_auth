/**
 *  @file
 **/

(function ($, Drupal, window, document, cookie) {

  'use strict';
  const AUTH_COOKIE = 'STYXKEY_pelcro_user_auth_token'
  const USER_COOKIE = 'STYXKEY_pelcro_user_id'
  const REFRESH_COOKIE = 'STYXKEY_pelcro_refresh'

  cookie.remove(REFRESH_COOKIE)

  //  A user has logged in through the Pelcro JS SDK
  document.addEventListener("PelcroUserLogin", function (e) {
    showOverlay("login")
    cookie.set(AUTH_COOKIE, btoa(e.detail.data.token), { expires: 14 })
    cookie.set(USER_COOKIE, e.detail.data.id)
    let destination = window.location.href
    let url = window.location.protocol + '//' + window.location.hostname + '/pelcro/login/' + btoa(e.detail.data.token) + '?origin=' + destination
    window.location.replace(url)
  });

  // A new user has registered.
  document.addEventListener("PelcroUserRegister", function (e) {
    cookie.set(AUTH_COOKIE, btoa(e.detail.data.token), { expires: 14 })
    cookie.set(USER_COOKIE, e.detail.data.id)
    $.getJSON("/pelcro/register/" + btoa(e.detail.data.token, function (data) {
      if (!data.success) {
        console.log("Error registering new user, message was " + data.message)
      }
    }))
  });

  // A user has bought a new subscription through the Pelcro JS SDK
  document.addEventListener("PelcroSubscriptionCreate", function (e) {
    showOverlay("update")
    cookie.set(AUTH_COOKIE, btoa(e.detail.data.token), { expires: 14 })
    cookie.set(USER_COOKIE, e.detail.data.id)
    const destination = (drupalSettings.pelcro.subscribeRedirect !== "") ? drupalSettings.pelcro.subscribeRedirect : window.location.href
    const url = window.location.protocol + '//' + window.location.hostname + '/pelcro/refresh/' + btoa(e.detail.data.token) + '?origin=' + destination
    window.location.replace(url)
  });

  // A user has updated their info in the Pelcro JS SDK
  document.addEventListener("PelcroUserUpdated", function (e) {
    cookie.set(AUTH_COOKIE, btoa(e.detail.data.token), { expires: 14 })
    cookie.set(USER_COOKIE, e.detail.data.id)
    $.getJSON("/pelcro/update/" + btoa(e.detail.data.token, function (data) {
      if (!data.success) {
        console.log("Error updating user, message was " + data.message)
      }
    }))
  });

  // A user has logged out in the Pelcro JS SDK
  document.addEventListener("PelcroUserLogout", function (e) {
    showOverlay("logout")
    cookie.remove(AUTH_COOKIE)
    cookie.remove(USER_COOKIE)
    let url = window.location.protocol + '//' + window.location.hostname + '/pelcro/logout'
    window.location.assign(url)
  });

  // The Pelcro registration workflow is complete.
  document.addEventListener("PelcroSyncUser", function (e) {
    showOverlay("update")
    cookie.set(AUTH_COOKIE, btoa(Pelcro.user.auth.token), { expires: 14 })
    const destination = (drupalSettings.pelcro.subscribeRedirect !== "") ? drupalSettings.pelcro.subscribeRedirect : window.location.href
    const url = window.location.protocol + '//' + window.location.hostname + '/pelcro/refresh/' + btoa(Pelcro.user.auth.token) + '?origin=' + destination
    window.location.replace(url)
  });

  function showOverlay(type) {
    const overlay = document.getElementById('pelcro-auth-overlay')
    overlay.classList.add(type);
    overlay.classList.add('pelcro-overlay-visible');
  }

})(jQuery, Drupal, this, this.document, this.Cookies);
