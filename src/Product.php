<?php

namespace Drupal\pelcro_auth;

/**
 * Represents a Pelcro Product.
 */
class Product {

  /**
   * The product id.
   *
   * @var int
   */
  protected int $id;

  /**
   * The product name.
   *
   * @var string
   */
  protected string $name;

  /**
   * The product description.
   *
   * @var string
   */
  protected string $description;

  /**
   * Constructs a Product object.
   *
   * @param int $id
   *   The pelcro product id.
   * @param string $name
   *   The product name.
   * @param string $description
   *   The product description.
   */
  public function __construct(int $id, string $name, string $description = '') {
    $this->id = $id;
    $this->name = $name;
    $this->description = $description;
  }

  /**
   * Gets the value of $Id.
   *
   * @return int
   *   The product id.
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * Sets the value of $id.
   *
   * @param int $id
   *   The product's id.
   *
   * @return $this
   */
  public function setId(int $id): Product {
    $this->id = $id;
    return $this;
  }

  /**
   * Gets the value of $Name.
   *
   * @return string
   *   The product's name.
   */
  public function getName(): string {
    return $this->name;
  }

  /**
   * Sets the value of $name.
   *
   * @param string $name
   *   The product's name.
   *
   * @return $this
   */
  public function setName(string $name): Product {
    $this->name = $name;
    return $this;
  }

  /**
   * Gets the value of $Description.
   *
   * @return string
   *   The product's description.
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * Sets the value of $description.
   *
   * @param string $description
   *   The product's description.
   *
   * @return $this
   */
  public function setDescription(string $description): Product {
    $this->description = $description;
    return $this;
  }

}
