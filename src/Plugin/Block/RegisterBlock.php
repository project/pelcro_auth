<?php

namespace Drupal\pelcro_auth\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\pelcro_auth\PelcroConnectorInterface;

/**
 * Provides a Pelcro Registration button block.
 *
 * @Block(
 *  id = "pelcro_register",
 *  admin_label = @Translation("Pelcro Registration"),
 * )
 */
class RegisterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $build['register'] = [
      '#type' => 'html_tag',
      '#value' => 'Register',
      '#tag' => 'button',
      '#attributes' => [
        'class' => [PelcroConnectorInterface::PELCRO_REGISTER],
      ],
    ];

    return $build;
  }

}
