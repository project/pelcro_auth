<?php

namespace Drupal\pelcro_auth\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pelcro_auth\PelcroConnectorInterface;

/**
 * Provides a 'SubscribeBlock' block.
 *
 * @Block(
 *  id = "pelcro_subscribe",
 *  admin_label = @Translation("Pelcro Subscribe"),
 * )
 */
class SubscribeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'button_label' => 'Subscribe',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Label'),
      '#description' => $this->t('Enter the label for this subscribe button'),
      '#default_value' => $this->configuration['button_label'] ?? '',
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['product_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Product ID'),
      '#description' => $this->t('(optional) Enter the product ID for this subscription'),
      '#default_value' => $this->configuration['product_id'] ?? NULL,
      '#weight' => '1',
    ];
    $form['plan_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Plan ID'),
      '#description' => $this->t('(optional) Enter the plan ID for this subscription'),
      '#default_value' => $this->configuration['plan_id'] ?? NULL,
      '#weight' => '2',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['button_label'] = $form_state->getValue('button_label');
    $this->configuration['product_id'] = $form_state->getValue('product_id');
    $this->configuration['plan_id'] = $form_state->getValue('plan_id');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $attributes = ['class' => [PelcroConnectorInterface::PELCRO_SUBSCRIBE]];
    if (!empty($this->configuration['product_id'])) {
      $attributes['data-product-id'] = $this->configuration['product_id'];
      if (!empty($this->configuration['plan_id'])) {
        $attributes['data-plan-id'] = $this->configuration['plan_id'];
      }
    }

    $build['subscribe'] = [
      '#type' => 'html_tag',
      '#value' => $this->configuration['button_label'],
      '#tag' => 'button',
      '#attributes' => $attributes,
    ];
    return $build;
  }

}
