<?php

namespace Drupal\pelcro_auth\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\pelcro_auth\PelcroConnectorInterface;

/**
 * Provides a 'LoginBlock' block.
 *
 * @Block(
 *  id = "pelcro_login",
 *  admin_label = @Translation("Pelcro Login"),
 * )
 */
class LoginBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $build['login'] = [
      '#type' => 'html_tag',
      '#value' => 'Login',
      '#tag' => 'button',
      '#attributes' => [
        'class' => [PelcroConnectorInterface::PELCRO_LOGIN],
      ],
    ];

    return $build;
  }

}
