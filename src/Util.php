<?php

namespace Drupal\pelcro_auth;

/**
 * Class to hold utility static functions.
 */
class Util {

  /**
   * Strips Pelcro modal triggers from destination.
   *
   * @param string $origin
   *   The url of the origin request.
   *
   * @return string
   *   The url to redirect to.
   */
  public static function cleanOrigin(string $origin): string {
    $modals = [
      'login',
      'register',
      'password-forgot',
    ];
    $parts = parse_url($origin);
    if (empty($parts['query'])) {
      return $origin;
    }
    $query = [];
    parse_str($parts['query'], $query);
    if (!isset($query['view'])) {
      return $origin;
    }
    if ($query['view'] === 'password-reset') {
      unset($query['view'], $query['email'], $query['token']);
    }
    elseif (in_array($query['view'], $modals)) {
      unset($query['view']);
    }
    else {
      return $origin;
    }
    return (isset($parts['scheme']) ? $parts['scheme'] . '://' : '')
      . ($parts['host'] ?? '')
      . (isset($parts['port']) ? ':' . $parts['port'] : '' )
      . ($parts['path'] ?? '/')
      . (!empty($query) ? '?' . http_build_query($query) : '')
      . (isset($parts['fragment']) ? '#' . $parts['fragment'] : '');
  }

}
