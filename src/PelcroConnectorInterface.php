<?php

namespace Drupal\pelcro_auth;

/**
 * Provides an interface for the pelcro connector service.
 */
interface PelcroConnectorInterface {

  const AUTH_COOKIE = 'STYXKEY_pelcro_user_auth_token';
  const USER_COOKIE = 'STYXKEY_pelcro_user_id';
  const REFRESH_COOKIE = 'STYXKEY_pelcro_refresh';

  const PELCRO_LOGIN = 'pelcro-login-button';
  const PELCRO_REGISTER = 'pelcro-register-button';
  const PELCRO_SUBSCRIBE = 'pelcro-subscribe-button';

  /**
   * The machine name of the provider.
   */
  const AUTH_PROVIDER = 'pelcro_auth';

  /**
   * Retrieves a user data array from an auth token.
   *
   * @return object
   *   The pelcro data object.
   */
  public function getUserData(): object;

  /**
   * Returns a list of available products for this site.
   *
   * @return array
   *   An array of pelcro data objects representing products available to this
   *   site.
   */
  public function listProducts(): array;

  /**
   * Determines whether we have a user token set in cookies.
   *
   * @return bool
   *   True if we have a user auth token available, False otherwise.
   */
  public function hasUserToken(): bool;

  /**
   * Determines if the site config is properly set up.
   *
   * @return bool
   *   True if we have a site id and site auth token available, False otherwise.
   */
  public function hasSiteConfig(): bool;

  /**
   * Retrieves the settings array for use in drupalSettings.
   *
   * @return array
   *   The settings array for use in drupalSettings, or an empty array if the
   *   site is not yet configured.
   */
  public function getSettings(): array;

  /**
   * Gets the configured server type.
   *
   * @return string
   *   The server type.
   */
  public function isProduction(): bool;

}
