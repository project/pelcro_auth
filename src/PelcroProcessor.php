<?php

namespace Drupal\pelcro_auth;

/**
 * Process data returned by the pelcro API.
 */
class PelcroProcessor {

  /**
   * Gets a list of active subscriptions and their expiration dates.
   *
   * @param object $data
   *   The Pelcro user data returned from the api.
   *
   * @return array
   *   An array of expiration dates keyed by product id.
   */
  public static function getActiveProducts(object $data): array {
    $products = [];
    $subscriptions = $data->subscriptions ?? [];
    $memberships = $data->memberships ?? [];
    foreach ($subscriptions as $subscription) {
      if ($subscription->status === 'active' && $id = ($subscription->plan->product->id ?? FALSE)) {
        $products[$id] = $subscription->expires_at;
      }
    }
    foreach ($memberships as $membership) {
      if (!empty($membership->subscription) && $membership->subscription->status === 'active' && $id = ($membership->subscription->plan->product->id ?? FALSE)) {
        $products[$id] = $membership->subscription->expires_at;
      }
    }
    return $products;
  }

  /**
   * Gets a list of entitlements for the user.
   *
   * @param object $data
   *   The Pelcro user object.
   *
   * @return array
   *   An array of entitlements.
   */
  public static function getEntitlements(object $data): array {
    $entitlements = [];
    $subscriptions = $data->subscriptions ?? [];
    $memberships = $data->memberships ?? [];
    foreach ($subscriptions as $subscription) {
      if ($subscription->status === 'active' && is_array($subscription->plan->entitlements)) {
        $entitlements = array_merge($entitlements, $subscription->plan->entitlements);
      }
    }
    foreach ($memberships as $membership) {
      if (!empty($membership->subscription) && $membership->subscription->status === 'active' && is_array($membership->subscription->plan->entitlements)) {
        $entitlements = array_merge($entitlements, $membership->subscription->plan->entitlements);
      }
    }
    return array_values(array_unique($entitlements));
  }

}
