<?php

namespace Drupal\pelcro_auth\Form;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\pelcro_auth\PelcroConnectorInterface;
use Drupal\pelcro_auth\Product;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for the Pelcro rolemap settings.
 */
class PelcroRoleMapForm extends ConfigFormBase {

  /**
   * The auth connector.
   *
   * @var \Drupal\pelcro_auth\PelcroConnectorInterface
   */
  protected PelcroConnectorInterface $pelcroConnector;

  /**
   * The user_role storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $roleStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    /** @var \Drupal\pelcro_auth\PelcroConnectorInterface $pelcro_auth_connector */
    $pelcro_auth_connector = $container->get('pelcro_auth.connector');
    $form->pelcroConnector = $pelcro_auth_connector;
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    $form->roleStorage = $entity_type_manager->getStorage('user_role');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pelcro_auth.pelcrorolemap',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pelcro_rolemap_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Only show form if we can connect to Pelcro.
    if (!$this->pelcroConnector->hasSiteConfig()) {
      $settings_link = Link::createFromRoute(
        'here',
        'pelcro_auth.pelcro_settings_form',
        [
          'destination' => Url::fromRoute('pelcro_auth.pelcro_rolemap_form')->getInternalPath(),
        ]);
      $form['message'] = [
        '#type' => 'item',
        '#markup' => "<p>In order to configure role mappings, you must first configure the Pelcro authorization settings " . $settings_link->toString() . '.</p>',
      ];
      return $form;
    }
    $config = $this->config('pelcro_auth.pelcrorolemap');

    // Product section.
    $mapping_config = $config->get('role_map') ?? [];
    $mappings = [];
    foreach ($mapping_config as $mapping) {
      $mappings[$mapping['product_id']] = $mapping['role_ids'];
    }

    $products = $this->getProducts();
    $roles = $this->getRoles();
    $form['#tree'] = TRUE;

    $form['intro'] = [
      '#type' => 'item',
      '#markup' => "<p>Configure your product settings here.  For each product on pelcro, select the role you wish assigned to those with that product subscription.</p>",
    ];
    $form['role_maps'] = [];
    foreach ($products as $product) {
      $form['role_map'][$product->getId()] = [
        '#type' => 'checkboxes',
        '#title' => $product->getName(),
        '#description' => $product->getDescription(),
        '#options' => $roles,
        '#default_value' => $mappings[$product->getId()] ?? [],
      ];
    }

    // Entitlements section.
    $form['entitlement_map'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'entitlements-section',
      ],
      'items' => [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'entitlements-items',
        ],
      ],
    ];
    $entitlements =
      array_values($form_state->getValue(['entitlement_map', 'items'], $config->get('entitlement_map')) ?? []);
    $entitlement_count = $form_state->get('entitlement_count') ?? count($entitlements) + 1;

    if ($element = $form_state->getTriggeringElement()) {
      if (mb_strpos($element['#id'], 'edit-entitlement-map-add') === 0) {
        $entitlement_count += 1;
      }
    }
    $form_state->set('entitlement_count', $entitlement_count);
    for ($i = 0; $i < $entitlement_count; $i++) {
      $form['entitlement_map']['items'][$i] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'entitlement-' . $i,
        ],
        'entitlement' => [
          '#type' => 'textfield',
          '#title' => $this->t('Entitlement'),
          '#description' => $this->t('Text string of entitlement, exactly as entered in pelcro'),
          '#maxlength' => 128,
          '#size' => 64,
          '#default_value' => $entitlements[$i]['entitlement'] ?? '',
        ],
        'role_ids' => [
          '#type' => 'checkboxes',
          '#title' => $this->t('Roles'),
          '#description' => $this->t('Choose roles to be received from this entitlement'),
          '#options' => $roles,
          '#default_value' => $entitlements[$i]['role_ids'] ?? [],
        ],
      ];
    }
    $form['entitlement_map']['add'] = [
      '#type' => 'button',
      '#value' => $this->t('Add Entitlement'),
      '#ajax' => [
        'callback' => '::entitlementUpdate',
        'event' => 'click',
        'wrapper' => 'entitlements-section',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Prepare rolemap.
    $role_mappings = [];
    foreach ($form_state->getValue('role_map') as $product_id => $role_ids) {
      if ($role_ids) {
        $role_mappings[] = [
          'role_ids' => array_keys(array_filter($role_ids)),
          'product_id' => (int) $product_id,
        ];
      }
    }

    // Prepare entitlementmap.
    $entitlement_mappings = [];
    foreach ($form_state->getValue(['entitlement_map', 'items']) as $item) {
      if (!empty($item['entitlement'])) {
        $entitlement_mappings[] = [
          'entitlement' => $item['entitlement'],
          'role_ids' => array_keys(array_filter($item['role_ids'])),
        ];
      }
    }
    // Save config.
    $this->config('pelcro_auth.pelcrorolemap')
      ->set('role_map', $role_mappings)
      ->set('entitlement_map', $entitlement_mappings)
      ->save();
  }

  /**
   * Ajax callback to add new entitlement.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The new render array.
   */
  public function entitlementUpdate(array &$form, FormStateInterface $form_state): array {
    return $form['entitlement_map'];

  }

  /**
   * Retrieve a list of products to map from the Pelcro API.
   *
   * @return \Drupal\pelcro_auth\Product[]
   *   An array of products, keyed by product id.
   */
  protected function getProducts(): array {
    $products_data = $this->pelcroConnector->listProducts();
    $products = [];
    foreach ($products_data as $product) {
      $products[$product->id] = new Product($product->id, $product->name, $product->description ?? '');
    }
    return $products;
  }

  /**
   * Retrieves a list of user roles for use as options.
   *
   * @return array
   *   An array of user roles, suitable for a dropdown options menu.
   */
  protected function getRoles(): array {
    $roles = [];
    /** @var \Drupal\user\Entity\Role[] $role_data */
    $role_data = $this->roleStorage->loadMultiple();
    // Ignore anonymous and authenticated roles.
    unset($role_data[AccountInterface::ANONYMOUS_ROLE]);
    unset($role_data[AccountInterface::AUTHENTICATED_ROLE]);
    foreach ($role_data as $role) {
      if ($role->hasPermission('bypass pelcro auth')) {
        continue;
      }
      $roles[$role->id()] = $role->label();
    }
    return $roles;
  }

}
