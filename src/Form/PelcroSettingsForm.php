<?php

namespace Drupal\pelcro_auth\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RequestContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for the basic Pelcro settings.
 */
class PelcroSettingsForm extends ConfigFormBase {

  /**
   * Mapping for default plecro account keys.
   */
  const PELCRO_USER_FIELDS = [
    'display_name' => "Display Name",
    'email' => 'Email',
    'first_name' => "First Name",
    'last_name' => "Last Name",
    'name' => 'Full Name',
    'phone' => 'Phone Number',
    'title' => 'Title',
  ];

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected RequestContext $requestContext;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);

    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager */
    $field_manager = $container->get('entity_field.manager');
    $form->entityFieldManager = $field_manager;

    /** @var \Drupal\Core\Routing\RequestContext $request_context */
    $request_context = $container->get('router.request_context');
    $form->requestContext = $request_context;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'pelcro_auth.pelcrosettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pelcro_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pelcro_auth.pelcrosettings');
    $mapping_config = $config->get('field_mapping') ?? [];
    $mappings = [];
    foreach ($mapping_config as $mapping) {
      $mappings[$mapping['field_name']] = $mapping['account_key'];
    }

    $form['#tree'] = TRUE;
    $form['server'] = [
      '#type' => 'radios',
      '#title' => $this->t('Pelcro Server'),
      '#description' => $this->t('Select whether to use staging or production credentials. You must have the proper account to use for each.'),
      '#options' => [
        'www' => $this->t('Production'),
        'staging' => $this->t('Staging'),
      ],
      '#default_value' => $config->get('server'),
    ];
    $form['site_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pelcro Site ID'),
      '#description' => $this->t('Your Site ID, which you can find at the end of the URL for your site on the Pelcro platform in the web browser&#039;s address bar (e.g. https://pelcro.com/admin/dashboard/XX), where XX is your Site ID.'),
      '#maxlength' => 16,
      '#size' => 16,
      '#default_value' => $config->get('site_id'),
    ];
    $form['api_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Access Token API key'),
      '#description' => $this->t('Your access token api key can be retrieved through your dashboard. It should be kept secret, and should not be entered here if you commit config to your repository. It should be managed in whichever way you manage secrets for your system.'),
      '#default_value' => $config->get('api_key'),
    ];
    $form['subscribe_redirect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('New Subscription Redirect'),
      '#default_value' => $config->get('subscribe_redirect'),
      '#size' => 40,
      '#description' => $this->t('Optionally, specify a relative URL to redirect users to after purchasing a new subscription.'),
      '#field_prefix' => $this->requestContext->getCompleteBaseUrl(),
    ];
    $form['environment_ui'] = [
      '#type' => 'url',
      '#title' => $this->t('Environment UI'),
      '#description' => $this->t('Optional, if you were provided with a custom javascript file for your site, enter the url here.'),
      '#default_value' => $config->get('environment_ui'),
    ];
    $form['environment_stripe'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment Stripe'),
      '#description' => $this->t('Provided by pelcro'),
      '#maxlength' => 128,
      '#size' => 64,
      '#default_value' => $config->get('environment_stripe'),
    ];
    $form['enable_name_fields_in_register'] = [
      '#type' => 'checkbox',
      '#title' => "Enable additional name fields in register dialog",
      '#default_value' => $config->get('enable_name_fields_in_register'),
    ];

    $form['field_mappings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('User field mappings'),
    ];

    foreach ($this->getMappableUserFields() as $field_name => $field_label) {
      $form['field_mappings'][$field_name] = [
        '#type' => 'select',
        '#title' => $field_label,
        '#options' => [0 => "Do not map to this field"] + self::PELCRO_USER_FIELDS,
        '#default_value' => $mappings[$field_name] ?? 0,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $mappings = [];
    foreach ($form_state->getValue('field_mappings') as $field_name => $account_key) {
      if ($account_key) {
        $mappings[] = [
          'field_name' => $field_name,
          'account_key' => $account_key,
        ];
      }
    }
    $this->config('pelcro_auth.pelcrosettings')
      ->set('server', $form_state->getValue('server'))
      ->set('site_id', $form_state->getValue('site_id'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('subscribe_redirect', $form_state->getValue('subscribe_redirect'))
      ->set('enable_name_fields_in_register', (bool) $form_state->getValue('enable_name_fields_in_register'))
      ->set('environment_ui', $form_state->getValue('environment_ui'))
      ->set('environment_stripe', $form_state->getValue('environment_stripe'))
      ->set('field_mapping', $mappings)
      ->save();
  }

  /**
   * Retrieve mappable user fields from the field manager.
   *
   * @return array
   *   Array of field labels keyed by field names.
   */
  protected function getMappableUserFields(): array {
    $fields = [];
    $all_definitions = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    $base_definitions = $this->entityFieldManager->getBaseFieldDefinitions('user');
    $bundle_definitions = array_diff_key($all_definitions, $base_definitions);
    /** @var \Drupal\field\FieldConfigInterface $definition */
    foreach ($bundle_definitions as $definition) {
      if ($definition->getType() === 'string') {
        $fields[$definition->getName()] = $definition->getLabel();
      }
    }
    return $fields;
  }

}
