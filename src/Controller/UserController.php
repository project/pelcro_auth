<?php

namespace Drupal\pelcro_auth\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\externalauth\AuthmapInterface;
use Drupal\externalauth\ExternalAuthInterface;
use Drupal\pelcro_auth\PelcroConnectorInterface;
use Drupal\pelcro_auth\PelcroProcessor;
use Drupal\pelcro_auth\Util;
use Drupal\user\UserInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for the pelcro user journey.
 */
class UserController extends ControllerBase {

  /**
   * Drupal\pelcro_auth\PelcroConnectorInterface definition.
   *
   * @var \Drupal\pelcro_auth\PelcroConnectorInterface
   */
  protected PelcroConnectorInterface $pelcroAuthConnector;

  /**
   * Drupal\externalauth\AuthmapInterface definition.
   *
   * @var \Drupal\externalauth\AuthmapInterface
   */
  protected AuthmapInterface $externalauthAuthmap;

  /**
   * Drupal\externalauth\ExternalAuthInterface definition.
   *
   * @var \Drupal\externalauth\ExternalAuthInterface
   */
  protected ExternalAuthInterface $externalauthExternalauth;

  /**
   * The Pelcro Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * The pelcro configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $pelcroConfig;

  /**
   * The pelcro configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $pelcroRolemap;

  /**
   * Constructs a UserController object.
   *
   * @param \Drupal\pelcro_auth\PelcroConnectorInterface $pelcro_auth_connector
   *   The Pelcro connector service.
   * @param \Drupal\externalauth\AuthmapInterface $externalauth_authmap
   *   The external_auth authmap service.
   * @param \Drupal\externalauth\ExternalAuthInterface $externalauth_externalauth
   *   The external_auth external_auth service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The pelcro logger.
   */
  public function __construct(PelcroConnectorInterface $pelcro_auth_connector, AuthmapInterface $externalauth_authmap, ExternalAuthInterface $externalauth_externalauth, AccountProxyInterface $current_user, ConfigFactoryInterface $config_factory, LoggerChannelInterface $logger) {
    $this->pelcroAuthConnector = $pelcro_auth_connector;
    $this->externalauthAuthmap = $externalauth_authmap;
    $this->externalauthExternalauth = $externalauth_externalauth;
    $this->currentUser = $current_user;
    $this->pelcroConfig = $config_factory->get('pelcro_auth.pelcrosettings');
    $this->pelcroRolemap = $config_factory->get('pelcro_auth.pelcrorolemap');
    $this->logger = $logger;

    // If pelcro isn't configured, don't do anything.
    if (!$this->pelcroAuthConnector->hasSiteConfig()) {
      $this->logger->warning("Pelcro Site configuration missing. Please add your site id and auth token to the Pelcro settings page.");
      throw new NotFoundHttpException();
    }

    // If the logged in used bypasses the pelcro authentication or is able to
    // edit the pelcro settings, don't do anything. Ideally anyone with
    // 'configure pelcro settings' would have 'bypass pelcro auth' as well, but
    // we check both permissions to ensure someone with
    // 'configure pelcro settings' but not explicitly having
    // 'bypass pelcro auth' won't be logged out and locked out when they do the
    // initial pelcro setup.
    if ($this->currentUser->hasPermission("bypass pelcro auth")
      || $this->currentUser->hasPermission("configure pelcro settings")) {
      throw new NotFoundHttpException();
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pelcro_auth.connector'),
      $container->get('externalauth.authmap'),
      $container->get('externalauth.externalauth'),
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('logger.channel.pelcro_auth'),
    );
  }

  /**
   * User login controller.
   *
   * Responsible for logging a user in via pelcro api and redirecting them to
   * the original page or the homepage.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $api_key
   *   The pelcro api_key.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the appropiate page.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function login(Request $request, string $api_key): RedirectResponse {
    if ($this->currentUser->isAnonymous()) {

      // At this point we know we need to update the user based on new data from
      // Pelcro, so go ahead and retrieve it.  If the call fails, we can't do
      // anything else, so log the error and return.
      try {
        $pelcro_user_data = $this->pelcroAuthConnector->getUserData();
        $user = $this->userLogin($pelcro_user_data);
        $this->logger->debug("Successfully logged in %user via pelcro", ['%user' => $user->getAccountName()]);
      }
      catch (ClientException $e) {
        $this->logger->error("Pelcro Auth call failed. Message was %error", ['%error' => $e->getMessage()]);
      }
    }
    else {
      $this->logger->warning("Attempted to log in a user via pelcro, but a user was alredy logged in");
    }
    if ($destination = $request->query->get('origin')) {
      return new RedirectResponse(Util::cleanOrigin($destination));
    }
    return $this->redirect('<front>');
  }

  /**
   * Asynchronously registers a new user account via the pelcro api.
   *
   * @param string $api_key
   *   The pelcro api key.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A json response with data on the success or failure.
   */
  public function register(string $api_key): JsonResponse {
    $message = '';
    $success = TRUE;
    if ($this->currentUser->isAnonymous()) {
      try {
        $pelcro_user_data = $this->pelcroAuthConnector->getUserData();
        if (!$this->externalauthAuthmap->getUid($pelcro_user_data->id, PelcroConnectorInterface::AUTH_PROVIDER)) {
          $this->userRegister($pelcro_user_data);
        }
        else {
          $success = FALSE;
          $message = "Attempted to register an account that already exists.";
          $this->logger->error($message);
        }
      }
      catch (ClientException $e) {
        $success = FALSE;
        $message = 'Pelcro Auth call failed.';
        $this->logger->error("Pelcro Auth call failed. Message was %error", ['%error' => $e->getMessage()]);
      }

    }
    else {
      $message = "User attempted to register new account while logged in";
      $this->logger->warning($message);
      $success = FALSE;
    }
    return new JsonResponse([
      'success' => $success,
      'message' => $message,
    ]);
  }

  /**
   * Updates the user data asynchronously.
   *
   * @param string $api_key
   *   The pelcro api key.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A json response with data on the success or failure.
   */
  public function update(string $api_key): JsonResponse {
    $success = TRUE;
    $message = "";
    try {
      $pelcro_user_data = $this->pelcroAuthConnector->getUserData();
      if ($this->externalauthAuthmap->getUid($pelcro_user_data->id, PelcroConnectorInterface::AUTH_PROVIDER) == $this->currentUser()->id()) {
        $this->userUpdate($pelcro_user_data);
      }
      else {
        $success = FALSE;
        $message = "Attempted to update a user which is not logged in.";
        $this->logger->error($message);
      }
    }
    catch (ClientException $e) {
      $success = FALSE;
      $message = 'Pelcro Auth call failed.';
      $this->logger->error("Pelcro Auth call failed. Message was %error", ['%error' => $e->getMessage()]);
    }
    return new JsonResponse([
      'success' => $success,
      'message' => $message,
    ]);
  }

  /**
   * Updates the user data and refreshes the page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $api_key
   *   The pelcro api key.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect to the original page.
   */
  public function refresh(Request $request, string $api_key): RedirectResponse {
    try {
      $pelcro_user_data = $this->pelcroAuthConnector->getUserData();
      if ($this->externalauthAuthmap->getUid($pelcro_user_data->id, PelcroConnectorInterface::AUTH_PROVIDER) == $this->currentUser()->id()) {
        $this->userUpdate($pelcro_user_data);
      }
      else {
        $this->logger->error("Attempted to update a user which is not logged in.");
      }
    }
    catch (ClientException $e) {
      $this->logger->error("Pelcro Auth call failed. Message was %error", ['%error' => $e->getMessage()]);
    }
    if ($destination = $request->query->get('origin')) {
      return new RedirectResponse(Util::cleanOrigin($destination));
    }
    return $this->redirect('<front>');
  }

  /**
   * Logs a user out and redirects to the homepage.
   *
   * This replicates going to /user/logout, but we have a custom controller
   * here just in case a site has disabled or moved that route.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect to the homepage.
   */
  public function logout(): RedirectResponse {
    if ($this->currentUser->isAuthenticated()) {
      $this->logger->debug("Logging out %user via Pelcro", ['%user' => $this->currentUser->getAccountName()]);
      user_logout();

    }
    return $this->redirect('<front>');
  }

  /**
   * Logs a user in based on the Pelcro data and updates the user if necessary.
   *
   * @param object $data
   *   The pelcro user object returned from the api.
   *
   * @return \Drupal\user\UserInterface
   *   The logged in user.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function userLogin(object $data): UserInterface {
    // The user exists, so log them in and update their account.
    if ($this->externalauthAuthmap->getUid($data->id, PelcroConnectorInterface::AUTH_PROVIDER)) {
      $this->userUpdate($data);
      return $this->externalauthExternalauth->login($data->id, PelcroConnectorInterface::AUTH_PROVIDER);
    }

    // We are creating a new user.
    return $this->userRegister($data);
  }

  /**
   * Registers a user with a unique username.
   *
   * @param object $data
   *   The user data.
   *
   * @return \Drupal\user\UserInterface
   *   The new user.
   */
  private function userRegister(object $data): UserInterface  {
    $account_data = $this->getAccountData($data);
    //check for existing user
    $name = $account_data['name'];
    $i = 0;
    while (user_load_by_name($name)) {
      $name = $account_data['name'] . '_' . ++$i;
    }
    $account_data['name'] = $name;
    $account = $this->externalauthExternalauth->register($data->id, PelcroConnectorInterface::AUTH_PROVIDER, $account_data);
    return $this->externalauthExternalauth->userLoginFinalize($account, $data->id, PelcroConnectorInterface::AUTH_PROVIDER);

  }

  /**
   * Updates the user based on pelcro api data.
   *
   * @param object $data
   *   The Pelcro user object returned from the api.
   *
   * @return \Drupal\user\UserInterface
   *   The updated user.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function userUpdate(object $data): UserInterface {
    $account_data = $this->getAccountData($data);
    $user = $this->externalauthExternalauth->load($data->id, PelcroConnectorInterface::AUTH_PROVIDER);
    unset($account_data['name']);
    foreach ($account_data as $key => $value) {
      $user->set($key, $value);
    }
    $user->save();
    return $user;
  }

  /**
   * Gets the account data this user should have according to the pelcro api.
   *
   * @param object $data
   *   The pelcro user object returned from the api.
   *
   * @return array
   *   An array of account data, keyed by field id.
   */
  protected function getAccountData(object $data): array {
    $account_data = [
      'name' => $data->email,
      'mail' => $data->email,
    ];
    $user_field_mapping_config = $this->pelcroConfig->get('field_mapping') ?? [];
    foreach ($user_field_mapping_config as $mapping) {
      $account_data[$mapping['field_name']] = $data->{$mapping['account_key']} ?? NULL;
    }
    $account_data['roles'] = $this->getRoles($data);
    return $account_data;
  }

  /**
   * Gets a list of roles this user should have according to the pelcro data.
   *
   * @param object $data
   *   The pelcro user object returned from the api.
   *
   * @return array
   *   An array of role ids that this user should have.
   */
  private function getRoles(object $data): array {
    $active_products = array_keys(PelcroProcessor::getActiveProducts($data));
    $entitlements = PelcroProcessor::getEntitlements($data);
    $product_map = $this->getProductMapping();
    $entitlement_map = $this->getEntitlementMapping();
    $roles = [];
    foreach ($active_products as $product_id) {
      $roles = array_merge($roles, array_diff($product_map[$product_id] ?? [], $roles));
    }
    foreach ($entitlements as $entitlement) {
      $roles = array_merge($roles, array_diff($entitlement_map[$entitlement] ?? [], $roles));
    }
    return $roles;
  }

  /**
   * Gets an array of configured rolemappings from the raw config data.
   *
   * @return array
   *   An array where the keys are product ids, and the values are arrays of
   *   Drupal role machine names.
   */
  private function getProductMapping(): array {
    $map = [];
    $role_map = $this->pelcroRolemap->get('role_map') ?? [];
    foreach ($role_map as $mapping) {
      $map[$mapping['product_id']] = $mapping['role_ids'];
    }
    return $map;
  }

  /**
   * Gets an array of configured rolemappings from the raw config data.
   *
   * @return array
   *   An array where the keys are product ids, and the values are arrays of
   *   Drupal role machine names.
   */
  private function getEntitlementMapping(): array {
    $map = [];
    $role_map = $this->pelcroRolemap->get('entitlement_map') ?? [];
    foreach ($role_map as $mapping) {
      $map[$mapping['entitlement']] = $mapping['role_ids'];
    }
    return $map;
  }

}
