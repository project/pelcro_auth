<?php

namespace Drupal\pelcro_auth;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Pelcro Connection service.
 *
 * This service should handle all requests to the Pelcro API via endpoint
 * specific public methods on the interface. In general, methods may throw a
 * PelcroException, meaning the that Pelcro configuration is not valid for the
 * request, or the parameters for the call are incorrect. The call may also
 * throw a Guzzle client exception if the call results in something other than
 * a 200 response.
 */
class PelcroConnector implements PelcroConnectorInterface {

  /**
   * Pelcro settings configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The Guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The Pelcro site ID as set in config.
   *
   * @var string
   */
  protected string $siteId = '';

  /**
   * The base API URL.
   *
   * @var string
   */
  protected string $server = '';

  /**
   * The Pelcro site authorization token, as provided in config.
   *
   * @var string
   */
  protected string $siteToken = '';

  /**
   * The Pelcro user auth token, as found in the cookies.
   *
   * @var string
   */
  protected string $userToken = '';

  /**
   * The Pelcro API version to use. Currently v1.
   *
   * @var string
   */
  protected string $version = 'v1';

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Constructs a new PelcroConnector object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, RequestStack $request_stack, LoggerChannelInterface $logger) {
    $this->logger = $logger;
    $request = $request_stack->getCurrentRequest();
    if ($request->cookies->has(self::AUTH_COOKIE)) {
      $this->userToken = base64_decode($request->cookies->get(self::AUTH_COOKIE));
    }
    $this->config = $config_factory->get('pelcro_auth.pelcrosettings');
    $this->siteId = $this->config->get('site_id') ?? '';
    $server = $this->config->get('server') === 'www' ? 'www' : 'staging';
    $this->server = sprintf("https://%s.pelcro.com/api/%s/", $server, $this->version);
    $this->siteToken = $this->config->get('api_key') ?? '';
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public function hasUserToken(): bool {
    return (bool) $this->userToken;
  }

  /**
   * {@inheritdoc}
   */
  public function hasSiteConfig(): bool {
    return $this->siteId && $this->siteToken;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserData(): object {
    $response = $this->postSdk('customer');
    return $response->data;
  }

  /**
   * {@inheritdoc}
   */
  public function listProducts(): array {
    $data = $this->getCore('products');
    return $data->data;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings(): array {
    if (!$this->hasSiteConfig()) {
      return [];
    }
    return [
      'siteid' => $this->config->get('site_id'),
      'stripe' => $this->config->get('environment_stripe'),
      'subscribeRedirect' => $this->config->get('subscribe_redirect'),
      'domain' => $this->config->get('server') === 'staging' ? "https://staging.pelcro.com" : "https://www.pelcro.com",
      'ui' => $this->config->get('environment_ui'),
      'enableNameFieldsInRegister' => $this->config->get('enable_name_fields_in_register') ?? FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isProduction(): bool {
    return $this->config->get('server') === 'www';
  }

  /**
   * Retrieves data from a core Pelcro endpoint using the GET method.
   *
   * @param string $endpoint
   *   The endpoint.
   * @param array $query
   *   The query parameters to be appended to the request.
   *
   * @return object
   *   The Pelcro response data, JSON decoded.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Drupal\pelcro_auth\PelcroException
   */
  protected function getCore(string $endpoint, array $query = []): object {
    if (!$this->hasSiteConfig()) {
      throw new PelcroException('No site configuration available to make core request');
    }
    $headers = [
      'Authorization' => 'Bearer ' . $this->siteToken,
    ];
    return $this->call('GET', "core/$endpoint", $query, $headers);
  }

  /**
   * Retrieves data from a Pelcro SDK endpoint using the POST method.
   *
   * @param string $endpoint
   *   The endpoint.
   * @param array $body_params
   *   Body parameters to be sent along with the request.
   *
   * @return object
   *   The Pelcro response data, JSON decoded.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Drupal\pelcro_auth\PelcroException
   */
  protected function postSdk(string $endpoint, array $body_params = []): object {
    // User Token is required for SDK calls.
    if (!$this->hasUserToken()) {
      throw new PelcroException("No user available to make sdk request.");
    }
    $body = '';
    $query = [
      'auth_token' => $this->userToken,
    ];
    $headers = [];
    if ($body_params) {
      $headers['Content-Type'] = 'application/json';
      $body = json_encode($body_params);
    }
    return $this->call('POST', "sdk/$endpoint", $query, $headers, $body);
  }

  /**
   * Retrieves data from the Pelcro API.
   *
   * @param string $method
   *   The method to use in making this call.
   * @param string $endpoint
   *   The endpoint to call.
   * @param array $query
   *   The query to be appended to the end of the URL.
   * @param array $headers
   *   The headers to include in the call.
   * @param string $body
   *   The query body for a post request.
   *
   * @return object
   *   The returned Pelcro data object.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Drupal\pelcro_auth\PelcroException
   */
  protected function call(string $method, string $endpoint, array $query = [], array $headers = [], string $body = ''): object {
    $options = [];

    // The site_id query parameter is required on all calls.
    if (empty($this->siteId)) {
      throw new PelcroException("Site Id must be configured on the module settings page");
    }
    $options[RequestOptions::QUERY] = $query + [
      'site_id' => $this->siteId,
    ];

    // The 'Accept' header is required on all calls.
    $options[RequestOptions::HEADERS] = $headers + [
      'Accept' => 'application/json',
    ];

    if ($body) {
      $options[RequestOptions::BODY] = $body;
    }

    $response = $this->httpClient->request($method, $this->server . $endpoint, $options);
    return json_decode($response->getBody());
  }

}
